extern crate editdistance;

#[test]
fn simple() {
    assert_eq!(editdistance::editdistance("kitten".chars(), "sitting".chars()), 3);
    assert_eq!(editdistance::editdistance("Tier".chars(), "Tor".chars()), 2);
}

#[test]
fn same() {
    assert_eq!(editdistance::editdistance("kitten".chars(), "kitten".chars()), 0);
}

#[test]
fn empty_a() {
    assert_eq!(editdistance::editdistance("".chars(), "kitten".chars()), 6);
}

#[test]
fn empty_b() {
    assert_eq!(editdistance::editdistance("sitting".chars(), "".chars()), 7);
}

#[test]
fn empty_both() {
    assert_eq!(editdistance::editdistance("".chars(), "".chars()), 0);
}

#[test]
fn unicode_misc() {
    assert_eq!(editdistance::editdistance("üö".chars(), "uo".chars()), 2);
}

#[test]
fn unicode_thai() {
    assert_eq!(editdistance::editdistance("ฎ ฏ ฐ".chars(), "a b c".chars()), 3);
}

#[test]
fn unicode_misc_equal() {
    assert_eq!(editdistance::editdistance("☀☂☃☄".chars(), "☀☂☃☄".chars()), 0);
}

extern crate quickcheck;
use quickcheck::quickcheck;

#[test]
fn at_least_size_difference_property() {
    fn at_least_size_difference(a: String, b: String) -> bool {
        let size_a = a.chars().count() as i32;
        let size_b = b.chars().count() as i32;
        let diff = (size_a - size_b).abs();

        editdistance::editdistance(a.chars(), b.chars()) >= diff
    }

    quickcheck(at_least_size_difference as fn(a: String, b: String) -> bool);
}

#[test]
fn at_most_length_of_longer_property() {
    fn at_most_size_of_longer(a: String, b: String) -> bool {
        let upper_bound = *[a.chars().count(),
                            b.chars().count()]
            .iter()
            .max()
            .unwrap() as i32;
        editdistance::editdistance(a.chars(), b.chars()) <= upper_bound
    }

    quickcheck(at_most_size_of_longer as fn(a: String, b: String) -> bool);
}

#[test]
fn zero_iff_a_equals_b_property() {
    fn zero_iff_a_equals_b(a: String, b: String) -> bool {
        let d = editdistance::editdistance(a.chars(), b.chars());

        if a == b {
            d == 0
        } else {
            d > 0
        }
    }

    quickcheck(zero_iff_a_equals_b as fn(a: String, b: String) -> bool);
}

#[test]
fn triangle_inequality_property() {
    fn triangle_inequality(a: String, b: String, c: String) -> bool {
        editdistance::editdistance(a.chars(), b.chars()) <=
            editdistance::editdistance(a.chars(), c.chars()) +
            editdistance::editdistance(b.chars(), c.chars())
    }

    quickcheck(triangle_inequality as fn(a: String, b: String, c: String) -> bool);
}
