extern crate lifeguard;

pub use c_api::*;

use lifeguard::Pool;
use std::cmp::min;

pub fn editdistance<'a, I, T>(root: I, query: I) -> i32
    where I: IntoIterator<Item = T>,
          T: PartialEq,
          T: 'a,
          I::IntoIter: Clone
{
    let root_iter = root.into_iter();
    let query_iter = query.into_iter();
    let (s1, s2) = match (root_iter.size_hint().1, query_iter.size_hint().1) {
        (Some(r), Some(q)) => {
            if r > q {
                (query_iter, root_iter)
            } else {
                (root_iter, query_iter)
            }
        }
        _ => (root_iter, query_iter),
    };

    let pool: Pool<Vec<i32>> = Pool::with_size(2);
    let initial = pool.new_from(0..(s1.clone().count() as i32 + 1)).detach();
    *s2.enumerate()
        .fold(initial, move |distances, (i2, c2)| {
            let ret = pool.new_from(Some(i2 as i32 + 1).into_iter().chain(s1.clone()
                    .zip(distances.windows(2))
                    .scan(i2 as i32 + 1, |last, (c1, distance_pair)| {
                        let (d1, d2) = (distance_pair[0], distance_pair[1]);
                        Some(_compare(&c1, &c2, d1, d2, last))
                    })))
                .detach();
            pool.attach(distances);
            ret
        })
        .last()
        .expect("Unreachable")
}

fn _compare<T: PartialEq>(c1: &T, c2: &T, d1: i32, d2: i32, last: &mut i32) -> i32 {
    if c1 == c2 {
        d1
    } else {
        *last = if d1 < d2 { d1 } else { min(d2, *last) } + 1;
        *last
    }
}

mod c_api {
    extern crate libc;
    use std::ffi::CStr;
    use std::str;
    use super::editdistance;

    #[no_mangle]
    pub unsafe extern "C" fn editdistance_checked(root: *const libc::c_char,
                                                  query: *const libc::c_char)
                                                  -> libc::int32_t {
        let (root, query) = {
            (CStr::from_ptr(root).to_bytes(), CStr::from_ptr(query).to_bytes())
        };
        editdistance(str::from_utf8(root).unwrap().chars(),
                     str::from_utf8(query).unwrap().chars())
    }

    #[no_mangle]
    pub unsafe extern "C" fn editdistance_unchecked(root: *const libc::c_char,
                                                    query: *const libc::c_char)
                                                    -> libc::int32_t {
        let (root, query) = {
            (str::from_utf8_unchecked(CStr::from_ptr(root).to_bytes()),
             str::from_utf8_unchecked(CStr::from_ptr(query).to_bytes()))
        };
        editdistance(root.chars(), query.chars())
    }
}
