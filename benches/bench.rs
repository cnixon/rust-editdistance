#![feature(test)]
extern crate test;
extern crate rand;

extern crate editdistance;

use test::Bencher;
use rand::{Rng,SeedableRng};
use rand::distributions::{IndependentSample, Range};

use editdistance::editdistance;

#[bench]
fn setup_random_strings(b: &mut Bencher) {
    let mut rng = rand::StdRng::from_seed(&[0]);
    let mut rng2 = rand::StdRng::from_seed(&[0]);
    let between = Range::new(0, 500);

    let val = (1..100).into_iter().map(|_|{rng.gen_ascii_chars()
                                           .take(between.ind_sample(&mut rng2))
                                           .collect::<String>()}).collect::<Vec<String>>();

    let vals = val.iter().zip(val.iter().skip(1));

    b.iter(|| { vals.clone().map(| (a, b)| {editdistance(a.chars(), b.chars())}).fold(0, |x, y|{x+y}) })

}
